# Hooks

Allow others the ability to extend your package

## Installation

Install the package from composer.

```
composer install rapture/hooks
```

## Usage

Within your package

@hook
@filter

Hook::dispatch();
Filter::dispatch();

Available to other developers

Hook::attach();
Filter::attach();
