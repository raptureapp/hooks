<?php

namespace Rapture\Hooks;

use Illuminate\Support\Facades\Event;
use Rapture\Hooks\Contracts\PrioritizedEvents;

class Filters implements PrioritizedEvents
{
    protected $events;

    public function __construct()
    {
        $this->events = collect([]);
    }

    public function dispatch($event, $payload = [])
    {
        $events = $this->events->filter(function ($item) use ($event) {
            return $item['event'] === $event;
        })->sortBy('priority');

        if ($events->count() > 0) {
            foreach ($events as $index => $item) {
                if (!is_object($payload)) {
                    $payload = [$payload];
                }

                Event::listen($item['event'].$index, $item['listener']);
                $payload = Event::dispatch($event.$index, $payload, true);
            }
        }

        return $payload;
    }

    public function attach($event, $listener, $priority = 20)
    {
        $this->events->push([
            'event' => $event,
            'listener' => $listener,
            'priority' => $priority,
        ]);
    }
}
