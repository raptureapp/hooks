<?php

namespace Rapture\Hooks;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Rapture\Hooks\Facades\Filter;
use Rapture\Hooks\Facades\Hook;

class HookServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('hook', function ($expression) {
            return "<?php \Rapture\Hooks\Facades\Hook::dispatch({$expression}); ?>";
        });

        Blade::directive('filter', function ($expression) {
            return "<?php echo \Rapture\Hooks\Facades\Filter::dispatch({$expression}); ?>";
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('hook', function ($app) {
            return new Hooks();
        });

        $this->app->singleton('filter', function ($app) {
            return new Filters();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['hook', 'filter'];
    }
}
