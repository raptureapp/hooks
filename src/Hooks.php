<?php

namespace Rapture\Hooks;

use Illuminate\Support\Facades\Event;
use Rapture\Hooks\Contracts\PrioritizedEvents;

class Hooks implements PrioritizedEvents
{
    protected $events;

    public function __construct()
    {
        $this->events = collect([]);
    }

    public function dispatch($event, $payload = [])
    {
        $events = $this->events->filter(function ($item) use ($event) {
            return $item['event'] === $event;
        })->sortBy('priority')->each(function ($item, $index) {
            Event::listen($item['event'], $item['listener']);
        });

        if (!is_object($payload)) {
            $payload = [$payload];
        }

        Event::dispatch($event, $payload);
    }

    public function attach($event, $listener, $priority = 20)
    {
        $this->events->push([
            'event' => $event,
            'listener' => $listener,
            'priority' => $priority,
        ]);
    }
}
