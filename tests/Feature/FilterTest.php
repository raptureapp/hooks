<?php

use Rapture\Hooks\Facades\Filter;

it('can filter', function () {
    Filter::attach('test', fn($value) => 'test');

    $result = Filter::dispatch('test', 'nope');

    expect($result)->toEqual('test');
});

it('can extend', function () {
    Filter::attach('extend', fn($value) => $value . 'extended');

    $result = Filter::dispatch('extend', 'initial');

    expect($result)->toEqual('initialextended');
});

it('overwrites filters', function () {
    Filter::attach('overwrite', fn($value) => '1');
    Filter::attach('overwrite', fn($value) => '2');

    $result = Filter::dispatch('overwrite', 'nope');

    expect($result)->toEqual('2');
});

it('stacks filters', function () {
    Filter::attach('stack', fn($value) => '1');
    Filter::attach('stack', fn($value) => $value . '2');

    $result = Filter::dispatch('stack', 'nope');

    expect($result)->toEqual('12');
});

it('can prioritize', function () {
    Filter::attach('priority', fn($value) => $value . '2', 10);
    Filter::attach('priority', fn($value) => $value . '3', 5);

    $result = Filter::dispatch('priority', '1');

    expect($result)->toEqual('132');
});

it('can use arrays', function () {
    Filter::attach('arrays', fn($value) => array_merge($value, [3, 4]));

    $result = Filter::dispatch('arrays', [1, 2]);

    expect($result)->toMatchArray([1, 2, 3, 4]);
});

it('can be used in views', function () {
    Filter::attach('inline', fn($value) => 'replaced');

    $view = $this->blade('@filter("inline", "start")');

    $view->assertSee('replaced');
});

it('can be used multiple times', function () {
    Filter::attach('duplicate', fn($value) => 'replaced');

    $first = Filter::dispatch('duplicate', 'first');
    $second = Filter::dispatch('duplicate', 'second');

    expect($first)->toEqual('replaced');
    expect($second)->toEqual('replaced');
});

it('can be attached between uses', function () {
    Filter::attach('sequential', fn($value) => 'replaced');

    $first = Filter::dispatch('sequential', 'first');

    Filter::attach('sequential', fn($value) => 'again');

    $second = Filter::dispatch('sequential', 'second');

    expect($first)->toEqual('replaced');
    expect($second)->toEqual('again');
});
