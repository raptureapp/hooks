<?php

use Rapture\Hooks\Facades\Hook;

it('can attach a hook', function () {
    Hook::attach('inline', function ($value) {
        echo 'test';
    });

    $view = $this->blade('@hook("inline")');

    $view->assertSee('test');
});

it('can attach multiple hooks', function () {
    Hook::attach('multiple', function ($value) {
        echo '1';
    });

    Hook::attach('multiple', function ($value) {
        echo '2';
    });

    $view = $this->blade('@hook("multiple")');

    $view->assertSee('12');
});

it('can be prioritized', function () {
    Hook::attach('priority', function ($value) {
        echo '1';
    }, 10);

    Hook::attach('priority', function ($value) {
        echo '2';
    }, 5);

    $view = $this->blade('@hook("priority")');

    $view->assertSee('21');
});

it('can complete actions', function () {
    session()->put('action', null);

    Hook::attach('action', function ($value) {
        session()->put('action', $value);
    });

    Hook::dispatch('action', 1);

    expect(session()->get('action'))->toBe(1);
});

it('can be used multiple times', function () {
    Hook::attach('duplicate', function ($value) {
        echo '1';
    }, 10);

    Hook::attach('duplicate', function ($value) {
        echo '2';
    }, 20);

    $first = $this->blade('@hook("duplicate")');
    $second = $this->blade('@hook("duplicate")');

    $first->assertSee('12');
    $second->assertSee('12');
});
