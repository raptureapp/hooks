<?php

namespace Rapture\Hooks\Tests;

use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Rapture\Hooks\HookServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    use InteractsWithViews;

    protected function getPackageProviders($app)
    {
        return [
            HookServiceProvider::class,
        ];
    }
}
